package com.exampleapp.io.stepapp;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MyModel {
    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("pages")
    public ArrayList<PagesObject> pages;
}

class PagesObject {
    @SerializedName("label")
    public String label;

    @SerializedName("sections")
    public List<SectionsObject> sections;

    @Override
    public String toString () {
        return this.label ;
    }
}

class SectionsObject {
    @SerializedName("label")
    public String label;

    @SerializedName("elements")
    public ArrayList<ElementsObject> elements;
}

class ElementsObject{
    @SerializedName("type")
    public String type;

    @SerializedName("file")
    public String file;

    @SerializedName("unique_id")
    public String unique_id;

    @SerializedName("label")
    public String label;

    @SerializedName("isMandatory")
    public Boolean isMandatory;

    @SerializedName("keyboard")
    public String keyboard;

    @SerializedName("formattedNumeric")
    public String formattedNumeric;

    @SerializedName("mode")
    public String mode;

    @SerializedName("rules")
    public ArrayList<RulesObject> rules;
}

class RulesObject{
    @SerializedName("condition")
    public String condition;

    @SerializedName("value")
    public String value;

    @SerializedName("action")
    public String action;

    @SerializedName("otherwise")
    public String otherwise;

    @SerializedName("targets")
    public ArrayList<String> targets;
}
