package com.exampleapp.io.stepapp;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.exampleapp.io.stepapp.helper.PageLayout;
import com.squareup.picasso.Picasso;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BaseFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BaseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BaseFragment extends Fragment implements BlockingStep {
    // TODO: Rename parameter arguments, choose names that match

    Button btnDatePicker;

    private int mYear, mMonth, mDay, mHour, mMinute;

    private List<EditText> editTextList = new ArrayList<EditText>();

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private PagesObject mPagesObject;
    private int mPosition;
    private LinearLayout linearLayout;

    private String content, numContent, dateContent, radioText;
    private String radioContent;
    private Boolean isItMandatory;

    private OnFragmentInteractionListener mListener;

    public BaseFragment() {}
    public static BaseFragment build(PagesObject pagesObject, int position) {
        // Required empty public constructor
        BaseFragment baseFragment = new BaseFragment();
        baseFragment.mPagesObject = pagesObject;
        baseFragment.mPosition = position;
        return baseFragment;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BaseFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BaseFragment newInstance(String param1, String param2) {
        BaseFragment fragment = new BaseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        LinearLayout linearLayout = new PageLayout(getContext()).build();
        LinearLayout pageLinearLayout = new PageLayout(getContext()).build();
        for(SectionsObject sectionsObject : this.mPagesObject.sections) {
            TextView sectionTV = new TextView(getContext());
            sectionTV.setText(sectionsObject.label);
            sectionTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f);

            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            llp.setMargins(0, 20, 20, 20);
            sectionTV.setLayoutParams(llp);

            pageLinearLayout.addView(sectionTV);

            for(final ElementsObject elementsObject : sectionsObject.elements) {
                //TODO: check elements type
                View v = null;
                final EditText editText = new EditText(getContext());
                switch (elementsObject.type) {
                    case "embeddedphoto" :
                        String imageUri = elementsObject.file;
                        ImageView imageView = new ImageView(getContext());
                        Picasso.with(getContext()).load(imageUri).resize(700, 400).into(imageView);
                        v = imageView;
                        break;
                    case "text" :
                        if (elementsObject.unique_id.equals("text_3")) {
                            editText.setVisibility(View.INVISIBLE);
                            v = editText;
                        } else {
                            isItMandatory = elementsObject.isMandatory;
                            v = editText(elementsObject.label, isItMandatory);
                        }
                        break;
                    case "formattednumeric" :
                        EditText numText = editText(elementsObject.label, elementsObject.isMandatory);
                        numText.setInputType(InputType.TYPE_CLASS_NUMBER);
                        v = numText;
                        break;
                    case "datetime" :
                        final Calendar c = Calendar.getInstance();
                        mYear = c.get(Calendar.YEAR);
                        mMonth = c.get(Calendar.MONTH);
                        mDay = c.get(Calendar.DAY_OF_MONTH);

                        final EditText deditText = editText(elementsObject.label, elementsObject.isMandatory);
                        deditText.setFocusable(false);
                        deditText.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DatePickerDialog dialog =  new DatePickerDialog(getContext(),
                                    new DatePickerDialog.OnDateSetListener() {
                                        @Override
                                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                            deditText.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                        }
                                    }, mYear, mMonth, mDay);
                                dialog.show();
                            }
                        });
                        v = deditText;
                        break;
                    case "yesno" :
                        TextView yesnoTV = new TextView(getContext());
                        yesnoTV.setText(elementsObject.label);
                        final RadioGroup rg = new RadioGroup(getContext());
                        rg.addView(yesnoTV);
                        for (final RulesObject rulesObject : elementsObject.rules) {
                            if (rulesObject.targets.get(0).equals("text_3"))
                                editText.setHint(sectionsObject.elements.get(1).label);
                            String route[] = {"Yes", "No"};
                            for (int i = 0; i < 2; i++) {
                                final RadioButton radioButton = new RadioButton(getContext());
                                radioButton.setText(String.valueOf(route[i]));
                                radioButton.setId(i);
                                rg.addView(radioButton);
                                if (i == 0)
                                    radioButton.setChecked(true);
                                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                                        if (checkedId == 0 && elementsObject.rules.get(checkedId).value.equals("Yes")) {
                                            editText.setVisibility(View.VISIBLE);
                                            radioContent = elementsObject.rules.get(checkedId).value;
                                        } else {
                                            editText.setVisibility(View.INVISIBLE);
                                            radioContent = "No";
                                        }
                                    }
                                });
                            }
                            rg.addView(editText);
                            v = rg;
                        }
                        break;
                    default:
                        Toast.makeText(getContext(), "Error with the view", Toast.LENGTH_LONG).show();
                        v = null;
                        break;
                }
                pageLinearLayout.addView(v);
            }
        }
        linearLayout.addView(pageLinearLayout);

        View view =  inflater.inflate(R.layout.fragment_base, container, false);
        view.setPadding(20,20,20,3);
        FrameLayout frameLayout = (FrameLayout)view.findViewById(R.id.frameLayout);
        frameLayout.addView(linearLayout);
        return frameLayout;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.goToNextStep();
            }
        }, 0L);
    }

    @Override
    public void onCompleteClicked(final StepperLayout.OnCompleteClickedCallback callback) {
      //  Toast.makeText(getContext(), "Form successfully filled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.goToPrevStep();
            }
        }, 0L);
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
    }

    @Override
    public void onError(@NonNull VerificationError error) {
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public abstract class TextValidator implements TextWatcher {
        private final TextView textView;

        public TextValidator(TextView textView) {
            this.textView = textView;
        }

        public abstract void validate(TextView textView, String text);

        @Override
        final public void afterTextChanged(Editable s) {
            String text = textView.getText().toString();
            validate(textView, text);
        }

        @Override
        final public void beforeTextChanged(CharSequence s, int start, int count, int after) { /* Don't care */ }

        @Override
        final public void onTextChanged(CharSequence s, int start, int before, int count) { /* Don't care */ }
    }

    private boolean isEmpty(EditText etText) {
        if(etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }


    private boolean isEmptyRadio(String etRadio) {
        if(etRadio.length() > 0)
            return false;
        return true;
    }

    private boolean isMandatoryEmpty(boolean isit, EditText etText) {
        if(isit && etText.getText().toString().trim().length() > 0)
            return false;
        return true;
    }

    private EditText editText(String hint, Boolean isItMandatory) {
        if(isItMandatory.equals(true)) {
            final EditText editText = new EditText(getContext());
            editText.setHint(hint);
            editText.addTextChangedListener(new TextValidator(editText) {
                @Override public void validate(TextView textView, String text) {
                    if(isEmpty(editText))
                        textView.setError(textView.getHint() + " is mandatory");
                }
            });
            editTextList.add(editText);
            return editText;
        }
        else{
            final EditText editText = new EditText(getContext());
            editText.setHint(hint);
            editTextList.add(editText);
            return editText;
        }
    }

}
