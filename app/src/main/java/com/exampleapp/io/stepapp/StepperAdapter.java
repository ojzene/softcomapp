package com.exampleapp.io.stepapp;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.google.gson.Gson;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class StepperAdapter extends AbstractFragmentStepAdapter {

    public StepperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        return BaseFragment.build(this.getModel().pages.get(position), position);
    }

    @Override
    public int getCount() {
        MyModel myModel = this.getModel();
        ArrayList<PagesObject> pagesObjects = myModel.pages;
        return pagesObjects.size();
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        MyModel myModel = this.getModel();
        ArrayList<PagesObject> pagesObjects = myModel.pages;
        return new StepViewModel.Builder(context)
                        .setTitle(pagesObjects.get(position).label)
                        .create();
    }

    private String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            return new String(bytes);
        } catch (IOException e) {
            return null;
        }
    }

    private MyModel getModel() {
        String myJson = inputStreamToString(context.getResources().openRawResource(R.raw.pet_adoption));
        return new Gson().fromJson(myJson, MyModel.class);
    }

}