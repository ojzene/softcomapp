package com.exampleapp.io.stepapp.helper;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import static android.view.ViewGroup.LayoutParams.FILL_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static android.widget.LinearLayout.HORIZONTAL;
import static android.widget.LinearLayout.VERTICAL;

public class PageLayout {
    private Context mContext;
    public PageLayout(Context context) {
        this.mContext = context;
    }
    public LinearLayout build () {
        LinearLayout linearLayout = new LinearLayout(this.mContext);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(FILL_PARENT, WRAP_CONTENT);
        linearLayout.setLayoutParams(params);
        linearLayout.setOrientation(VERTICAL);
        return linearLayout;
    }
}